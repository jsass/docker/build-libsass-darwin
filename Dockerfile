ARG OSXCROSS_VERSION=latest
FROM --platform=$BUILDPLATFORM docker.io/crazymax/osxcross:${OSXCROSS_VERSION}-ubuntu AS osxcross

FROM docker.io/ubuntu
MAINTAINER "Tristan Lins" <tristan@lins.io>

COPY --from=osxcross /osxcross /osxcross
ENV PATH="/osxcross/bin:$PATH"
ENV LD_LIBRARY_PATH="/osxcross/lib:$LD_LIBRARY_PATH"

COPY install-temurin.sh /bin

# Install Wget, Java, Git and Build Tools
RUN set -x \
    && apt-get update \
    && apt-get install -y wget curl ca-certificates ca-certificates-java git build-essential libssl-dev clang libclang-dev \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/* /var/cache/apt/archives/*

# Install cmake 3.21
RUN set -x \
    && DIR=$(mktemp -d) \
    && cd "$DIR" \
    && wget -nv --no-check-certificate http://www.cmake.org/files/v3.21/cmake-3.21.2.tar.gz -O cmake-3.21.2.tar.gz \
    && tar xf cmake-3.21.2.tar.gz \
    && cd cmake-3.21.2 \
    && ./configure \
    && make -j8 \
    && make install \
    && cd / \
    && rm -rf "$DIR"

# Install Java JDK 8
RUN set -x \
    && install-temurin.sh -f 8 -o linux -a x64 \
    && cd /usr/local/include \
    && ln -s /opt/java8/include/*.h /opt/java8/include/linux/*.h .

# Setup binaries and java home path
ENV PATH /opt/java8/bin:$PATH
ENV HOME /tmp
ENV JAVA_HOME /opt/java8

# Check java and gradle work properly
RUN set -x \
    && java -version

# Install make scripts
COPY make-*.sh /

# Run configuration
WORKDIR /jsass
